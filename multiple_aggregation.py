class Book:
    def __init__(self, table_content_obj, chapter_obj):
        self.__table_content = table_content_obj
        self.__chapter = [chapter_obj]
        self.__glossary = []

    # chapters methods
    def add_chapter(self, chapter_obj):
        self.__chapter.append(chapter_obj)

    def add_chapters(self, chapters):
        self.__chapter += chapters

    # glossary methods
    def add_glosary(self, glosary_obj):
        if len(self.__glossary) > 1:
            raise ValueError("You cant add more glossaries, the limit is one")
        self.__glossary.append(glosary_obj)

    def get_table_content(self):
        return self.__table_content

    def get_chapter(self):
        return self.__chapter

    def get_glossary(self):
        return self.__glossary


class Table_Of_Contents():
    pass


class Glossary():
    pass


class Chapter:
    def __init__(self):
        self.__footnotes = []  # chapter could be 0...n footnotes

    def add_footnote(self, footnote_obj):
        self.__footnotes.append(footnote_obj)

    def add_footnotes(self, foot_notes):
        self.__footnotes += foot_notes

    def get_footnote(self):
        return self.__footnote


class Footnote:
    pass


chapter_obj = Chapter()
footnote_obj = Footnote()
table_of_content_obj = Table_Of_Contents()
glossary_obj = Glossary()
# add footnote

chapter_obj.add_footnote(footnote_obj)
book_obj = Book(table_of_content_obj, chapter_obj)
# print(book_obj.get_chapter()[0].get_footnote())
# print(book_obj.get_table_content())
# print(book_obj.get_chapter())
book_obj.add_glosary(glossary_obj)
print(book_obj.get_glossary())
print(book_obj.get_chapter())
book_obj.add_chapter(chapter_obj)
book_obj.add_chapter(chapter_obj)
book_obj.add_chapter(chapter_obj)
book_obj.add_chapter(chapter_obj)
book_obj.add_chapters([chapter_obj for _ in range(20)])
print(book_obj.get_chapter())
