class InsuranceInfo():
    def __init__(self, policy_name: str, policy_id: str):
        self.policy_name = policy_name
        self.policy_id = policy_id


class AddressInfo():
    def __init__(self, street: str, city: str, state: str, postal_code: str):
        self.street = street
        self.city = city
        self.state = state
        self.postal_code = postal_code

# main class


class Employee():
    def __init__(self, name: str, insurance: InsuranceInfo):
        self.name = name
        self.__address = AddressInfo(
            'street 3 av', 'new city', 'united state', '00000')
        self.__insurance = insurance

    def get_address(self):
        return self.__address

    def get_insurance(self):
        return self.__insurance


insurance_obj = InsuranceInfo('test', 'test')
employee_obj = Employee('Jose', insurance_obj)

print(employee_obj)
print(employee_obj.get_address().street)
