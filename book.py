class Chapter:
    pass


class Book:
    def __init__(self):
        self.chapters = [Chapter()]

    def add_chapter(self):
        self.chapters.append(Chapter())

    def add_chapters(self, number_of_chapters):
        if int(number_of_chapters) < 0:
            raise ValueError("Number of chapters could be more or equal to 1")
        self.chapters += [Chapter() for _ in range(int(number_of_chapters))]


book_obj = Book()
print(book_obj.chapters)
book_obj.add_chapters()
print(book_obj.chapters)
