class Department:
    pass


class Student:
    pass


# main class
class College():
    def __init__(self):
        self.__department = []
        self.__student = []

    # Students methods
    def add_student(self, student_obj):
        # control
        if not type(student_obj) == Student:
            raise ValueError("Student object has to be Student type")
        self.__student.append(student_obj)

    def add_students(self, students):
        self.__student += students

    # Departments Methods
    def add_department(self):
        self.__department.append(Department())

    def add_departments(self, number_of_department):
        if number_of_department < 0:
            raise ValueError('The number of departmets could be mor that 0')
        self.__department += [Department()
                              for _ in range(number_of_department)]

    # Getters
    def get_department(self):
        return self.__department

    def get_student(self):
        return self.__student


student_obj = Student()
college_obj = College()

print(college_obj)

college_obj.add_departments(5)
# print(college_obj.get_department())
college_obj.add_department()
# print(college_obj.get_department())

college_obj.add_student(student_obj)
print(college_obj.get_student())
college_obj.add_students([student_obj for _ in range(3)])
print(college_obj.get_student())
# college_obj.add_student([student_obj for _ in range(3)])
# print(college_obj.get_student())
